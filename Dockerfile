FROM gradle:jdk8
COPY build/classes/java/main/ /tmp
WORKDIR /tmp
ENTRYPOINT ["java", "Main"]