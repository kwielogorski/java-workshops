package patterns;

public class Car extends Vehicle {
    public Car(int numberOfWheels, boolean hasMotor) {
        this.numberOfWheels = numberOfWheels;
        this.hasMotor = hasMotor;
    }
}
