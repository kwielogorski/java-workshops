package patterns;

public class MonoCycle extends Vehicle {
    public MonoCycle(int numberOfWheels, boolean hasMotor) {
        this.numberOfWheels = numberOfWheels;
        this.hasMotor = hasMotor;
    }
}
