package patterns;

public class Bicycle extends Vehicle {
    public Bicycle(int numberOfWheels, boolean hasMotor) {
        this.numberOfWheels = numberOfWheels;
        this.hasMotor = hasMotor;
    }
}
