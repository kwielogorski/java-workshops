package patterns;

public class Motorbike extends Vehicle {
    public Motorbike(int numberOfWheels, boolean hasMotor) {
        this.numberOfWheels = numberOfWheels;
        this.hasMotor = hasMotor;
    }
}
