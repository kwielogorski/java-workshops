package patterns;

public class ChainOfResponsibilityMain {

    static public void run() {
        Vehicle chain = createChain();

        chain.ride(4, true);
    }

    static private Vehicle createChain() {
        Vehicle mono = new MonoCycle(1, false);
        Vehicle bicycle = new Bicycle(2, false);
        Vehicle motorbike = new Motorbike(2, true);
        Vehicle car = new Car(4,true);

        mono.setNextVehicle(bicycle);
        bicycle.setNextVehicle(motorbike);
        motorbike.setNextVehicle(car);

        return mono;
    }
}
