package patterns;

public abstract class Vehicle {
    protected int numberOfWheels;
    protected boolean hasMotor;
    protected Vehicle nextVehicle;

    public void setNextVehicle(Vehicle vehicle) {
        nextVehicle = vehicle;
    }

    public void ride(int numberOfWheels, boolean hasMotor) {
        if (this.numberOfWheels == numberOfWheels && this.hasMotor == hasMotor) {
            System.out.println("I am riding " + this.toString());
        } else {
            chooseNextVehicle(numberOfWheels, hasMotor);
        }
    }

    public void chooseNextVehicle(int numberOfWheels, boolean hasMotor) {
        if (null != this.nextVehicle){
            this.nextVehicle.ride(numberOfWheels, hasMotor);
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": { numberOfWheels="+numberOfWheels+", hasMotor="+hasMotor+" }";
    }
}
