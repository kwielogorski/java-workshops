package collections;

public class Word implements Comparable<Word> {

    private final String word;

    public Word(String word) {
        this.word = word;
    }

    @Override
    public String toString() {
        return word;
    }

    @Override
    public int compareTo(Word w) {
        return word.compareTo(w.toString());
    }
}
