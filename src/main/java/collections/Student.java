package collections;

public class Student {
    final private String name;
    final private String surname;
    final private int points;

    public Student(String name, String surname, int points) {
        this.name = name;
        this.surname = surname;
        this.points = points;
    }

    public int getPoints() {
        return points;
    }

    @Override
    public String toString() {
        return "Student = { name="+name+", surname="+surname+", points="+points+"}";
    }
}
