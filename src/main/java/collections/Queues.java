package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;

public class Queues {

    private final Random r;

    private final PriorityQueue<String> queue;
    private final List<Word> words;

    public Queues() {
        r = new Random();
        queue = new PriorityQueue<String>();
        words = new ArrayList<Word>();
    }

    public void process() {
        generateListOfRandomStrings();

        for (Word word : words) {
            System.out.println("Added to queue: "+word.toString());
            queue.add(word.toString());
            printQueue();

            if (queue.size() >= 10) {
                String topEl = queue.poll();
                System.out.println("Size limit reached. Took one off from the top: " + topEl);
            }
        }
    }

    private void printQueue()
    {
        System.out.println("-- Printing queue");
        for (String el : queue) {
            System.out.println(el);
        }
        System.out.println("-- end");
    }

    private void generateListOfRandomStrings() {
        int listSize = 100;
        for (int i = 0; i < listSize; i++) {
            words.add(new Word(getRandomString(5)));
        }
        Collections.sort(words);
    }

    private String getRandomString(int stringLen) {
        StringBuilder buffer = new StringBuilder(stringLen);
        for (int i = 0; i < stringLen; i++) {
            int randomLimitedInt = 97 + (int) (r.nextFloat() * (122 - 97 + 1));
            buffer.append((char) randomLimitedInt);
        }


        return buffer.toString();
    }

}
