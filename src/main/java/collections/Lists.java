package collections;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class Lists {

    LinkedList<Integer> list;
    Random r;

    public Lists() {
        list = new LinkedList<Integer>();
        r = new Random();
    }

    public void processList() {
        int elements = r.nextInt(1000);
        for (int i = 0; i < elements; i++) {
            list.add(r.nextInt(2000));
        }
        workOnList();
    }

    public void workOnList() {
        int el = 0;
        int j = 0;
        int sum = 0;
        float avg = 0;
        Iterator<Integer> it = list.iterator();
        while (it.hasNext()) {
            el = it.next();
            System.out.println(j + "." + el);
            j++;
        }
        Collections.sort(list);
        it = list.iterator();
        j = 0;
        while (it.hasNext()) {
            el = it.next();
            System.out.println(j + "." + el);
            j++;
            sum += el;
        }
        avg = (float) sum / list.size();

        System.out.println("Suma: " + sum);
        System.out.println("Srednia: " + avg);
        System.out.println("Rozmiar listy: " + list.size());
    }

}
