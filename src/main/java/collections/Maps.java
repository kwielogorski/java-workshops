package collections;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class Maps {

    final private HashMap<String, Student> students = new HashMap<>();

    public void process() {
        generate();

        float sum = 0;
        for(Map.Entry entry : students.entrySet()) {
            Student student = (Student) entry.getValue();
            System.out.println(entry.getKey()+": "+student.toString());
            sum += student.getPoints();
        }
        float avg = sum / students.size();

        System.out.println("Average number of points: "+avg);
    }

    private void generate() {
        Random r = new Random();
        for (int i = 0; i < 30; i++) {
            students.put(
                    UUID.randomUUID().toString(),
                    new Student(
                            "Name_"+i,
                            "Surname_"+i,
                            r.nextInt(50)
                    )
            );
        }
    }
}
