import arrays.ArrayApp;
import calculator.CalcApp;
import collections.CollectionApp;
import database.DatabaseApp;
import inheritance.InheritanceApp;
import patterns.ChainOfResponsibilityMain;
import swing.SwingApp;

import java.io.IOException;
import java.sql.SQLException;

public class Main {
    static public void main(String[] args) throws IOException, ClassNotFoundException {
//        new CalcApp().run();
//        ArrayApp.run();
//        InheritanceApp.run();
//        CollectionApp.run();
        try {
            new DatabaseApp().run();
        } catch (SQLException e) {
            System.out.println("Something went wrong: "+e.getMessage());
        }
//        ChainOfResponsibilityMain.run();
//        SwingApp.run();
    }
}
