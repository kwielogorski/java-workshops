package swing;

import java.awt.EventQueue;

import javax.swing.JFrame;

import swing.calculator.MainWindow;

public class SwingApp {
    public static void run() {
        EventQueue.invokeLater(
                new Runnable() {
                    @Override
                    public void run() {
                        new MainWindow();
                    }
                }
        );
    }
}
