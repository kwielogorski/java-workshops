package swing.calculator;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class MainWindow extends JFrame {
    public MainWindow() {

        /*
         * https://www.geeksforgeeks.org/java-swing-simple-calculator/
         * https://www.tutorialspoint.com/Create-a-simple-calculator-using-Java-Swing
         * https://docs.oracle.com/javase/tutorial/uiswing/layout/visual.html
         *
         */
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(
                (int) Math.round(screenSize.getWidth()/2)-110,
                (int) Math.round(screenSize.getHeight()/2-95),
                220,
                190
        );
        setTitle("Kalkulator WIT");
        setResizable(false);

        setLayout(new FlowLayout());

        JTextField digitalDisplay = new JTextField("", 18);
        digitalDisplay.setSize(300, 40);
        digitalDisplay.setFont(new Font("Courier New", Font.BOLD, 17));
        digitalDisplay.setEditable(false);
        digitalDisplay.setHorizontalAlignment(JTextField.RIGHT);


        JPanel bPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        Calculator calc = new Calculator(digitalDisplay);

        JButton bClear = new JButton("C");
        JButton bEl = new JButton("√");
        bEl.setActionCommand("^1/");
        JButton bPow = new JButton("^");
        JButton bDel = new JButton("<");
        JButton b1 = new JButton("1");
        JButton b2 = new JButton("2");
        JButton b3 = new JButton("3");
        JButton b4 = new JButton("4");
        JButton b5 = new JButton("5");
        JButton b6 = new JButton("6");
        JButton b7 = new JButton("7");
        JButton b8 = new JButton("8");
        JButton b9 = new JButton("9");
        JButton b0 = new JButton("0");
        JButton bDiv = new JButton("/");
        JButton bPlus = new JButton("+");
        JButton bMulti = new JButton("*");
        JButton bSub = new JButton("-");
        JButton bRes = new JButton("=");
        JButton bCom = new JButton(".");

        bClear.addActionListener(calc);
        bEl.addActionListener(calc);
        bPow.addActionListener(calc);
        bDel.addActionListener(calc);
        b1.addActionListener(calc);
        b2.addActionListener(calc);
        b3.addActionListener(calc);
        b4.addActionListener(calc);
        b5.addActionListener(calc);
        b6.addActionListener(calc);
        b7.addActionListener(calc);
        b8.addActionListener(calc);
        b9.addActionListener(calc);
        b0.addActionListener(calc);
        bDiv.addActionListener(calc);
        bPlus.addActionListener(calc);
        bMulti.addActionListener(calc);
        bSub.addActionListener(calc);
        bRes.addActionListener(calc);
        bCom.addActionListener(calc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0; gbc.gridy = 0; bPanel.add(bClear, gbc);
        gbc.gridx = 1; gbc.gridy = 0; bPanel.add(bEl, gbc);
        gbc.gridx = 2; gbc.gridy = 0; bPanel.add(bPow, gbc);
        gbc.gridx = 3; gbc.gridy = 0; bPanel.add(bDel, gbc);
        gbc.gridx = 0; gbc.gridy = 1; bPanel.add(b1, gbc);
        gbc.gridx = 1; gbc.gridy = 1; bPanel.add(b2, gbc);
        gbc.gridx = 2; gbc.gridy = 1; bPanel.add(b3, gbc);
        gbc.gridx = 3; gbc.gridy = 1; bPanel.add(bDiv, gbc);
        gbc.gridx = 0; gbc.gridy = 2; bPanel.add(b4, gbc);
        gbc.gridx = 1; gbc.gridy = 2; bPanel.add(b5, gbc);
        gbc.gridx = 2; gbc.gridy = 2; bPanel.add(b6, gbc);
        gbc.gridx = 3; gbc.gridy = 2; bPanel.add(bMulti, gbc);
        gbc.gridx = 0; gbc.gridy = 3; bPanel.add(b7, gbc);
        gbc.gridx = 1; gbc.gridy = 3; bPanel.add(b8, gbc);
        gbc.gridx = 2; gbc.gridy = 3; bPanel.add(b9, gbc);
        gbc.gridx = 3; gbc.gridy = 3; bPanel.add(bSub, gbc);
        gbc.gridx = 0; gbc.gridy = 4; bPanel.add(bCom, gbc);
        gbc.gridx = 1; gbc.gridy = 4; bPanel.add(b0, gbc);
        gbc.gridx = 2; gbc.gridy = 4; bPanel.add(bRes, gbc);
        gbc.gridx = 3; gbc.gridy = 4; bPanel.add(bPlus, gbc);

        add(digitalDisplay);
        add(bPanel);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
