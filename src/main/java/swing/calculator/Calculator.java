package swing.calculator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JTextField;

public class Calculator implements ActionListener {
    JTextField display;

    public Calculator(JTextField display) {
        this.display = display;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (command.charAt(0) == '=') {
            try {
                display.setText(this.calculate(display.getText()));
            } catch (ScriptException scriptException) {
                scriptException.printStackTrace();
            }
        } else if (command.charAt(0) == '<') {
            String displayText = display.getText();
            if (displayText.length() > 0) {
                displayText = displayText.substring(0, displayText.length()-1);
            }
            display.setText(displayText);
        } else if (command.charAt(0) == 'C') {
            display.setText("");
        } else {
            display.setText(display.getText() + command);
        }
    }

    private static String calculate(String expression) throws ScriptException {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");

        expression = expression.replaceAll("(([\\d.]+)[\\^]{1}([\\d.]+[/]?[\\d]*))", "Math.pow($2,$3)");
        System.out.println(expression);
        Object result = engine.eval(expression);

        return result.toString();
    }
}
