package inheritance;

public class FootballPlayer extends Person {
    private String position;
    private String footballClub;
    private int goalsCount = 0;

    public FootballPlayer(String name, String surname, String birthDate, String position, String footballClub) {
        super(name, surname, birthDate);

        this.position = position;
        this.footballClub = footballClub;
    }

    public void scoreGoal() {
        this.goalsCount++;
    }

    public void printInfo() {
        System.out.println(
            "Person = { name="+name+", surname="+surname+", birthDate="+birthDate+
                ", position="+position+", footballClub="+footballClub+", goalsCount="+goalsCount+" }"
        );
    }

}
