package inheritance;

public class InheritanceApp {
    public static void run() {
        Student student = new Student("Jan", "Kowalski", "2000-01-01", 2019, 12345);
        student.printInfo();

        FootballPlayer footballPlayer = new FootballPlayer(
            "Jan",
            "Kowalski",
            "2000-01-01",
            "bramkarz",
            "Arka Gdynia"
        );
        footballPlayer.scoreGoal();
        footballPlayer.scoreGoal();
        footballPlayer.scoreGoal();
        footballPlayer.printInfo();
    }
}
