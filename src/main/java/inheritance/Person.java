package inheritance;

public class Person {
    protected String name;
    protected String surname;
    protected String birthDate;

    Person(String name, String surname, String birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public void printInfo() {
        System.out.println("Person { name="+name+", surname="+surname+", birthDate="+birthDate+" }");
    }
}
