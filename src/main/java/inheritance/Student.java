package inheritance;

public class Student extends Person {
    private int year;
    private int indexNo;

    public Student(String name, String surname, String birthDate, int year, int indexNo) {
        super(name, surname, birthDate);

        this.year = year;
        this.indexNo = indexNo;
    }

    public void printInfo() {
        System.out.println(
            "Person { name="+name+", surname="+surname+", birthDate="+birthDate+", year="+year+", indexNo="+indexNo+" }"
        );
    }
}
