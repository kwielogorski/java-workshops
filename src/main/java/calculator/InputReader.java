package calculator;

import java.util.Scanner;

public class InputReader {
    public String read() {
        Scanner scanner = new Scanner(System.in);

        return scanner.nextLine();
    }
}
