package calculator;

import java.io.IOException;
import java.util.*;

public class CalcApp {

    private HashMap<String, Option> options = new HashMap<>();
    private InputReader inputReader = new InputReader();
    private Logger logger = new Logger();

    public CalcApp() throws IOException {
    }

    public void run() {
        loadOptions();
        loadAndDisplayOptions();
        runOption();
    }

    void loadOptions() {
        ServiceLoader<Option> serviceLoader = ServiceLoader.load(Option.class);
        for (Option option : serviceLoader) {
            options.put(option.getOptionKey(), option);
        }
    }

    void loadAndDisplayOptions() {
        System.out.println("Kwalkulator 0.1");
        for (Map.Entry<String, Option> entry : this.options.entrySet()) {
            Option option = (Option) entry.getValue();
            System.out.printf(
                "%-5s - %s\n",
                "[" + entry.getKey() + "]",
                option.getDescription()
            );
        }
        System.out.println("Wybierz opcję:");
    }

    void runOption() {
        String optionInput = inputReader.read();

        if (this.options.containsKey(optionInput)) {
            Option option = (Option) this.options.get(optionInput);
            option.takeInput();
            System.out.println("----");
            System.out.println("Wynik:");
            option.printResult();
            logger.logToFile(option.resultLine());
        }

        System.out.println("----");
        System.out.println("Bye.");
        System.exit(0);
    }
}
