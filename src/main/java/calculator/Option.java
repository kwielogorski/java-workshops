package calculator;

public interface Option {
    String getOptionKey();

    String getDescription();

    void takeInput();

    void printResult();

    String resultLine();
}
