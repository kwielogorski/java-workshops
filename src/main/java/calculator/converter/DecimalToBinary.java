package calculator.converter;

import calculator.InputReader;
import calculator.Option;

public class DecimalToBinary implements Option {
    private InputReader inputReader = new InputReader();

    private int x;

    @Override
    public String getOptionKey() {
        return "d2b";
    }

    @Override
    public String getDescription() {
        return "konwersja z dziesiętnego do binarnego";
    }

    @Override
    public void takeInput() {
        System.out.println("Podaj x:");
        this.x = new Integer(inputReader.read());
    }

    @Override
    public void printResult() {
        System.out.println(resultLine());
    }

    private String getResult() {
        return Integer.toBinaryString(x);
    }

    @Override
    public String resultLine() {
        return x + " -> " + getResult();
    }
}
