package calculator.converter;

import calculator.InputReader;
import calculator.Option;

public class BinaryToDecimal implements Option {
    private InputReader inputReader = new InputReader();

    private String x;

    public String getOptionKey() {
        return "b2d";
    }

    @Override
    public String getDescription() {
        return "konwersja z binarnego do dziesiętnego";
    }

    @Override
    public void takeInput() {
        System.out.println("Podaj x:");
        this.x = inputReader.read();
    }

    @Override
    public void printResult() {
        System.out.println(resultLine());
    }

    private int getResult() {
        return Integer.parseInt(x, 2);
    }

    @Override
    public String resultLine() {
        return x + " -> " + getResult();
    }
}
