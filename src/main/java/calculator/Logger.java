package calculator;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Logger {
    private FileWriter fileWriter;

    Logger() throws IOException {
        fileWriter = new FileWriter("wyniki.log", true);
    }

    public void logToFile(String text) {
        PrintWriter printWriter = new PrintWriter(fileWriter, true);
        printWriter.println(text);
        printWriter.close();
    }
}
