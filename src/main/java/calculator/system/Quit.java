package calculator.system;

import calculator.Option;

public class Quit implements Option {
    @Override
    public String getOptionKey() {
        return "q";
    }

    @Override
    public String getDescription() {
        return "wyjdź z programu";
    }

    @Override
    public void takeInput() {
    }

    @Override
    public void printResult() {
    }

    @Override
    public String resultLine() {
        return "quit";
    }
}
