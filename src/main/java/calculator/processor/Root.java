package calculator.processor;

import calculator.InputReader;
import calculator.Option;

public class Root implements Option {
    private InputReader inputReader = new InputReader();

    private double x;

    @Override
    public String getOptionKey() {
        return "r";
    }

    @Override
    public String getDescription() {
        return "pierwiastkowanie (Vx)";
    }

    @Override
    public void takeInput() {
        System.out.println("Podaj x:");
        this.x = new Double(inputReader.read());
    }

    @Override
    public void printResult() {
        System.out.println(resultLine());
    }

    private double getResult() {
        double squareRoot = 0.5;
        return Math.pow(x, squareRoot);
    }

    @Override
    public String resultLine() {
        return "V" + x + " = " + getResult();
    }
}
