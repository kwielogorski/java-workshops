package calculator.processor;

import calculator.InputReader;
import calculator.Option;

public class Power implements Option {
    private InputReader inputReader = new InputReader();

    private double x;
    private double p;

    @Override
    public String getOptionKey() {
        return "p";
    }

    @Override
    public String getDescription() {
        return "potęgowanie (x^p)";
    }

    @Override
    public void takeInput() {
        System.out.println("Podaj x:");
        this.x = new Double(inputReader.read());
        System.out.println("Podaj p:");
        this.p = new Double(inputReader.read());
    }

    @Override
    public void printResult() {
        System.out.println(resultLine());
    }

    private double getResult() {
        return Math.pow(x, p);
    }

    @Override
    public String resultLine() {
        return x + "^" + p + " = " + +getResult();
    }
}
