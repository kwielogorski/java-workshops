package calculator.processor;

import calculator.InputReader;
import calculator.Option;

public class Substraction implements Option {
    private InputReader inputReader = new InputReader();

    private int x;
    private int y;

    @Override
    public String getOptionKey() {
        return "s";
    }

    @Override
    public String getDescription() {
        return "odejmowanie (x - y)";
    }

    @Override
    public void takeInput() {
        System.out.println("Podaj x:");
        this.x = new Integer(inputReader.read());
        System.out.println("Podaj y:");
        this.y = new Integer(inputReader.read());
    }

    @Override
    public void printResult() {
        System.out.println(resultLine());
    }

    private int getResult() {
        return x - y;
    }

    @Override
    public String resultLine() {
        return x + " - " + y + " = " + getResult();
    }
}
