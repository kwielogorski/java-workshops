package calculator.processor.bit;

import calculator.InputReader;
import calculator.Option;

public class BitMoveRight implements Option {
    private InputReader inputReader = new InputReader();

    private int x;
    private int b;

    @Override
    public String getOptionKey() {
        return "br";
    }

    @Override
    public String getDescription() {
        return "przesunięcie bitowe w prawo (x >> b)";
    }

    @Override
    public void takeInput() {
        System.out.println("Podaj x:");
        this.x = new Integer(inputReader.read());
        System.out.println("Podaj b:");
        this.b = new Integer(inputReader.read());
    }

    @Override
    public void printResult() {
        System.out.println(resultLine());
    }

    private int getResult() {
        return x >> b;
    }

    @Override
    public String resultLine() {
        return x + " >> " + b + " = " + getResult();
    }
}
