package calculator.processor;

import calculator.InputReader;
import calculator.Option;

public class Addition implements Option {
    private InputReader inputReader = new InputReader();

    private int x;
    private int y;

    @Override
    public String getOptionKey() {
        return "a";
    }

    @Override
    public String getDescription() {
        return "dodawanie (x + y)";
    }

    @Override
    public void takeInput() {
        System.out.println("Podaj x:");
        this.x = new Integer(inputReader.read());
        System.out.println("Podaj y:");
        this.y = new Integer(inputReader.read());
    }

    @Override
    public void printResult() {
        System.out.println(resultLine());
    }

    @Override
    public String resultLine() {
        return x + " + " + y + " = " + getResult();
    }

    private int getResult() {
        return x + y;
    }
}
