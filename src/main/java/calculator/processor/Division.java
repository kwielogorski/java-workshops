package calculator.processor;

import calculator.InputReader;
import calculator.Option;

public class Division implements Option {
    private InputReader inputReader = new InputReader();

    private float x;
    private float y;

    @Override
    public String getOptionKey() {
        return "d";
    }

    @Override
    public String getDescription() {
        return "dzielenie (x / y)";
    }

    @Override
    public void takeInput() {
        System.out.println("Podaj x:");
        this.x = new Float(inputReader.read());
        System.out.println("Podaj y:");
        this.y = new Float(inputReader.read());
    }

    @Override
    public void printResult() {
        System.out.println(resultLine());
    }

    private float getResult() {
        if (y == 0) {
            System.out.println("Dzielenie przez 0 zabronione. Spróbuj jeszcze raz.");
            this.takeInput();
        }

        return x / y;
    }

    @Override
    public String resultLine
        () {
        return x + " / " + y + " = " + getResult();
    }
}
