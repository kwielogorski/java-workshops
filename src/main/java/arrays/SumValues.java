package arrays;

import java.util.Random;
import java.util.Scanner;

public class SumValues {
    public static void sum() {
        int n,m,max,skip;
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj pierwszy wymiar tablicy");
        n = in.nextInt();
        System.out.println("Podaj drugi wymiar tablicy");
        m = in.nextInt();
        System.out.println("Podaj skok do zliczenia elementow");
        skip = in.nextInt();
        System.out.println("Podaj maksymalna wartosc elementow w tablicy");
        max = in.nextInt();

        System.out.println("Zostanie wygenerowana macierz o wymiarach ["+n+","+m+"], maksymalnych wartosciach: "+
            max+", ze skokiem o wartosci: "+skip);

        int[][] matrix = new int[n][m];
        Random r = new Random();
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                matrix[i][j]=r.nextInt(max);
            }
        }
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                System.out.print(matrix[i][j]+" ");
            }
            System.out.print("\n");
        }
        int sum=0;
        int counter = skip+1;
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                if(counter==skip+1)
                {
                    sum+=matrix[i][j];
                    counter=0;
                    System.out.println("Sumuję element o wartości: "+matrix[i][j]);
                }
                counter++;
            }
        }
        System.out.println(sum);
    }
}
