package arrays;

import java.util.Random;
import java.util.Scanner;

public class MultiplyArrays {
    private Scanner in;
    private int aCols;
    private int aRows;
    private int aMax;
    private int bCols;
    private int bRows;
    private int bMax;

    public MultiplyArrays() {
        in = new Scanner(System.in);
    }

    public void multiply() {
        gatherInput();
        validate();

        int[][] matrixA = fillArray(aRows, aCols, aMax);
        int[][] matrixB = fillArray(bRows, bCols, bMax);

        calculate(matrixA, matrixB);
    }

    private void validate() {
        if (aCols != bRows) {
            System.out.println("Wymiary tablic nie pozwalają na ich przemnożenie. Drugi wymiar tablicy A musi być taki" +
                " sam jak pierwszy wymiar tablicy B");
            System.exit(0);
        }
    }

    private void gatherInput() {
        System.out.println("### Mnożenie macierzy: A*B");
        System.out.println("# Tablica A");
        System.out.println("podaj pierwszy wymiar tablicy A:");
        aRows = in.nextInt();
        System.out.println("podaj drugi wymiar tablicy A:");
        aCols = in.nextInt();
        System.out.println("podaj maksymalną wartość elementu w tablicy A:");
        aMax = in.nextInt();

        System.out.println("# Tablica B");
        System.out.println("podaj pierwszy wymiar tablicy B:");
        bRows = in.nextInt();
        System.out.println("podaj drugi wymiar tablicy B:");
        bCols = in.nextInt();
        System.out.println("podaj maksymalną wartość elementu w tablicy B:");
        bMax = in.nextInt();
    }

    private int[][] fillArray(int rows, int cols, int max) {
        Random rand = new Random();
        int[][] matrix = new int[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = rand.nextInt(max);
            }
        }

        return matrix;
    }

    private void printArray(int[][] matrix) {
        System.out.println("Printing array [" + matrix.length + "][" + matrix[0].length + "]");
        for (int[] row : matrix) {
            for (int val : row) {
                System.out.print(val + " ");
            }
            System.out.println("");
        }
    }

    private void calculate(int[][] a, int[][] b) {
        printArray(a);
        printArray(b);

        int[][] result = new int[a.length][b[0].length];
        for (int i = 0; i < a.length; i++) {

            for (int k = 0; k < b[0].length; k++) {
                int sum = 0;
                for (int j = 0; j < b.length; j++) {
                    // A[i][j] * B[j][k]

                    // A[0][0] * B[0][0] +
                    // A[0][1] * B[1][0] +
                    // A[0][2] * B[2][0] =
                    // A[0][0] * B[0][1] +
                    // A[0][1] * B[1][1] +
                    // A[0][2] * B[2][1] =

                    // A[1][0] * B[0][0] +
                    // A[1][1] * B[1][0] +
                    // A[1][2] * B[2][0] =
                    // A[1][0] * B[0][1] +
                    // A[1][1] * B[1][1] +
                    // A[1][2] * B[2][1] =
                    sum += a[i][j] * b[j][k];
                }

                result[i][k] = sum;
            }
        }

        printArray(result);
    }
}
