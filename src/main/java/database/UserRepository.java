package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserRepository {
    DBConnection db;

    public UserRepository() {
        db = new DBConnection();
    }

    public ResultSet getAll() throws SQLException {
        return db.query("select * from lab5");
    }

    public ResultSet getOne(int id) throws SQLException {
        PreparedStatement st = db.getConnection().prepareStatement("select * from lab5 where id = ?");
        st.setInt(1, id);
        return st.executeQuery();
    }

    public int add(String name, String surname, String nickname, int age, String pesel) throws SQLException {
        PreparedStatement st = db.getConnection().prepareStatement("insert into lab5 (Name, Surname, Nickname, Age, Pesel) values (?, ?, ?, ?, ?)");
        st.setString(1, name);
        st.setString(2, surname);
        st.setString(3, nickname);
        st.setInt(4, age);
        st.setString(5, pesel);
        int res = st.executeUpdate();
        st.close();

        return res;
    }

    public int update(int id, String name, String surname, String nickname, int age, String pesel) throws SQLException {
        PreparedStatement st = db.getConnection().prepareStatement("update lab5 set Name = ?, Surname = ?, Nickname = ?, Age = ?, Pesel = ? where id = ?");
        st.setString(1, name);
        st.setString(2, surname);
        st.setString(3, nickname);
        st.setInt(4, age);
        st.setString(5, pesel);
        st.setInt(6, id);
        int res = st.executeUpdate();
        st.close();

        return res;
    }

    public int delete(int id) throws SQLException {
        PreparedStatement st = db.getConnection().prepareStatement("delete from lab5 where id = ?");
        st.setInt(1, id);
        int res = st.executeUpdate();
        st.close();

        return res;
    }
}
