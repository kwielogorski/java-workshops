package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnection {
    Connection connection = null;

    public DBConnection() {
        connect();
    }

    public ResultSet query(String query) throws SQLException {
        Statement st = connection.createStatement();

        return st.executeQuery(query);
    }

    public Connection getConnection() {
        return connection;
    }

    private void connect() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String host = "jdbc:mysql://localhost:3306/java";
            String user = "root";
            String password = "password";
            connection = DriverManager.getConnection(host, user, password);
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("1: "+e.getMessage());
        }
    }

    private void disconnect() {
        try {
            if (connection != null) {
                connection.close();
                System.out.println("Connection to database properly closed!");
            }
        } catch (SQLException e) {
            System.out.println("2: "+e.getMessage());
        }
    }
}
