package database;

import java.sql.ResultSet;
import java.sql.SQLException;

import calculator.InputReader;

public class DatabaseApp {
    final private InputReader reader = new InputReader();
    final private UserRepository user = new UserRepository();

    public void run() throws SQLException {
//        DBTestConnection.test();

        while (true) {
            displayOptions();
            System.out.print("Podaj numer: ");
            String cmd = reader.read();

            switch (cmd) {
                case "1":
                    System.out.println("> Lista uzytkownikow");
                    ResultSet users = user.getAll();
                    while (users.next()) {
                        System.out.print("ID: "+users.getInt(1));
                        System.out.print(", Imie: "+users.getString(2));
                        System.out.print(", Nazwisko: "+users.getString(3));
                        System.out.print(", Nickname: "+users.getString(4));
                        System.out.print(", Wiek: "+users.getInt(5));
                        System.out.print(", Pesel: "+users.getString(6));
                        System.out.println("");
                    }
                    break;
                case "2":
                    addUser();
                    break;
                case "3":
                    editUser();
                    break;
                case "4":
                    deleteUser();
                    break;
                case "0":
                    System.out.println("Bye!");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Nie znaleziono polecenia.");
            }
        }
    }

    private void deleteUser() throws SQLException {
        System.out.println("> Usuwanie uzytkownika");
        System.out.print("Podaj ID uzytkownika, ktorego chcesz usunac: ");
        int id = Integer.parseInt(reader.read());
        System.out.println("");
        ResultSet u = user.getOne(id);
        if (!u.next()) {
            System.out.println("Uzytkownik nie istnieje");
            return;
        }
        user.delete(id);
        System.out.println("Uzytkownik usuniety!");
    }

    private void editUser() throws SQLException {
        System.out.println("> Edycja uzytkownika");
        System.out.print("Podaj ID uzytkownika, ktorego chcesz edytowac: ");
        int id = Integer.parseInt(reader.read());
        System.out.println("");
        ResultSet u = user.getOne(id);
        if (!u.next()) {
            System.out.println("Uzytkownik nie istnieje");
            return;
        }
        System.out.print("Imie ("+u.getString(2)+"): ");
        String name = reader.read();
        System.out.print("Nazwisko ("+u.getString(3)+"): ");
        String surname = reader.read();
        System.out.print("Nickname ("+u.getString(4)+"): ");
        String nickname = reader.read();
        System.out.print("Wiek ("+u.getString(5)+"): ");
        int age = Integer.parseInt(reader.read());
        System.out.print("Pesel ("+u.getString(6)+"): ");
        String pesel = reader.read();
        user.update(id, name, surname, nickname, age, pesel);
        System.out.println("Uzytkownik zaktualizowany!");
    }

    private void addUser() throws SQLException {
        System.out.println("> Dodawanie uzytkownika");
        System.out.print("Imie: ");
        String name = reader.read();
        System.out.print("Nazwisko: ");
        String surname = reader.read();
        System.out.print("Nickname: ");
        String nickname = reader.read();
        System.out.print("Wiek: ");
        int age = Integer.parseInt(reader.read());
        System.out.print("Pesel: ");
        String pesel = reader.read();
        System.out.println("");
        user.add(name, surname, nickname, age, pesel);
        System.out.println("Uzytkownik dodany!");
    }

    private static void displayOptions() {
        System.out.println("---");
        System.out.println("Wybierz:");
        System.out.println("1) lista");
        System.out.println("2) dodaj");
        System.out.println("3) edytuj");
        System.out.println("4) usun");
        System.out.println(" ");
        System.out.println("0) wyjdz");
    }
}
