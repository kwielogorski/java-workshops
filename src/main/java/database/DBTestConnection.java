package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBTestConnection {

    public static void test() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String host = "jdbc:mysql://localhost:3306/java";
            String user = "root";
            String password = "password";
            connection = DriverManager.getConnection(host, user, password);
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("1: "+e.getMessage());
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                    System.out.println("Connection to database properly closed!");
                }
            } catch (SQLException e) {
                System.out.println("2: "+e.getMessage());
            }
        }
    }

}
