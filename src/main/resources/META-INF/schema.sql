create database java;

create table lab5 (
    `ID` int not null auto_increment,
    `Name` varchar(45) not null,
    `Surname` varchar(45) not null,
    `Nickname` varchar(45) null,
    `Age` int null,
    `Pesel` varchar(11) null,
    primary key (`ID`)
);